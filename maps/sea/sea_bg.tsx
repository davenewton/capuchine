<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.1" name="sea_bg" tilewidth="1024" tileheight="576" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="1024" height="576" source="sea_background.png"/>
 </tile>
 <tile id="1">
  <image width="384" height="285" source="palace.png"/>
 </tile>
 <tile id="2">
  <image width="82" height="94" source="palace_door.png"/>
 </tile>
</tileset>
