<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.1" name="sea" tilewidth="32" tileheight="32" spacing="1" margin="1" tilecount="49" columns="7">
 <image source="sea.png" width="256" height="256"/>
 <tile id="6">
  <animation>
   <frame tileid="1" duration="300"/>
   <frame tileid="8" duration="300"/>
   <frame tileid="15" duration="300"/>
   <frame tileid="8" duration="100"/>
  </animation>
 </tile>
 <tile id="13">
  <animation>
   <frame tileid="0" duration="200"/>
   <frame tileid="7" duration="200"/>
   <frame tileid="14" duration="200"/>
   <frame tileid="21" duration="200"/>
  </animation>
 </tile>
</tileset>
