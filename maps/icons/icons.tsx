<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.1" name="icons" tilewidth="256" tileheight="67" tilecount="5" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <properties>
  <property name="doNotLoadBitmap" value="true"/>
 </properties>
 <tile id="1" class="capuchine">
  <image width="32" height="32" source="capuchine-icon.png"/>
 </tile>
 <tile id="2" class="turtle">
  <image width="72" height="67" source="turtle-icon.png"/>
 </tile>
 <tile id="3" class="capuchine_on_turtle">
  <image width="70" height="61" source="capuchine_on_turtle-icon.png"/>
 </tile>
 <tile id="4" class="jellyfish">
  <image width="47" height="42" source="jellyfish-icon.png"/>
 </tile>
 <tile id="5" class="capuchine_on_dragon">
  <image width="256" height="61" source="capuchine_on_dragon-icon.png"/>
 </tile>
</tileset>
