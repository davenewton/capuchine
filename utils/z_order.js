/**
 * Enum for Z order.
 * @readonly
 * @enum {number}
 */
const Z_ORDER = {
    MIN: 0,
    BACKGROUND: 0,
    RINGS: 16,
    CAPUCHINE: 17,
    TURTLE: 18,
    JELLYFISH: 19,
    DRAGON: 20,
    MAX: 20
};

export default Z_ORDER;