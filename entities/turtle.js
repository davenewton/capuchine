import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

class TurtleState {
    /**
     * @type Turtle
     */
    turtle;

    /**
     * @param {Turtle} turtle 
     */
    constructor(turtle) {
        this.turtle = turtle;
    }

    UpdateBody() {
    }

    UpdateSprite() {
        Playnewton.GPU.SetSpritePosition(this.turtle.sprite, this.turtle.body.position.x, this.turtle.body.position.y);
    }
}

class TurtleStuckState extends TurtleState {

    /**
     * @type number
     */
    stuckLevel = 120;

    /**
     * @param {Turtle} turtle 
     */
    constructor(turtle) {
        super(turtle);
        Playnewton.GPU.SetSpriteAnimation(this.turtle.sprite, Turtle.animations.stuck);
    }

    UpdateBody() {
        if (this.turtle.body.touches.any) {
            this.stuckLevel--;
        }
        if (this.stuckLevel <= 0) {
            this.turtle.state = new TurtleWalkState(this.turtle);
        }
    }
}

class TurtleWalkState extends TurtleState {
    /**
     * @param {Turtle} turtle 
     */
    constructor(turtle) {
        super(turtle);
        Playnewton.GPU.SetSpriteAnimation(this.turtle.sprite, Turtle.animations.walk);
        Playnewton.PPU.SetBodyImmovable(this.turtle.body, false);
        Playnewton.PPU.SetBodyVelocity(this.turtle.body, -this.turtle.walkSpeed, 0);
    }

    UpdateBody() {
        let limitX = Playnewton.PPU.world.bounds.left - this.turtle.body.width / 2;
        if (this.turtle.body.right < limitX) {
            this.turtle.state = new TurtleDiveState(this.turtle);
        }
    }
}

class TurtleDiveState extends TurtleState {
    /**
     * @param {Turtle} turtle 
     */
    constructor(turtle) {
        super(turtle);
        Playnewton.GPU.SetSpriteAnimation(this.turtle.sprite, Turtle.animations.dive, Playnewton.GPU_AnimationMode.ONCE);
        Playnewton.PPU.SetBodyImmovable(this.turtle.body, true);
    }

    UpdateSprite() {
        if (this.turtle.sprite.animationStopped) {
            this.turtle.state = new TurtleTalkState(this.turtle);
        }
    }
}

class TurtleTalkState extends TurtleState {
    /**
     * @param {Turtle} turtle 
     */
    constructor(turtle) {
        super(turtle);
    }
}


/**
 * 
 * @type TurtleAnimations
 */
class TurtleAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    stuck;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    walk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    dive;
}

export default class Turtle {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     *  @type number
     */
    walkSpeed = 1;

    /**
     * @type TurtleState
     */
    state;

    /**
     * @type boolean
     */
    get talking() {
        return this.state instanceof TurtleTalkState;
    }

    /**
     * @type TurtleAnimations
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "walk0", x: 1, y: 193, w: 71, h: 66 },
            { name: "walk1", x: 1, y: 260, w: 71, h: 66 },
            { name: "walk2", x: 1, y: 327, w: 71, h: 66 },
            { name: "walk3", x: 74, y: 193, w: 71, h: 66 },
            { name: "walk4", x: 74, y: 260, w: 71, h: 66 },
            { name: "walk5", x: 74, y: 327, w: 71, h: 66 },
            { name: "walk6", x: 147, y: 193, w: 71, h: 66 },
            { name: "walk7", x: 147, y: 260, w: 71, h: 66 },

            { name: "dive0", x: 147, y: 327, w: 71, h: 66 },
            { name: "dive1", x: 220, y: 193, w: 71, h: 66 },
            { name: "dive2", x: 220, y: 260, w: 71, h: 66 },

            { name: "stuck0", x: 220, y: 329, w: 71, h: 66 },
            { name: "stuck1", x: 293, y: 193, w: 71, h: 66 },
            { name: "stuck2", x: 293, y: 261, w: 71, h: 66 },
            { name: "stuck3", x: 293, y: 329, w: 71, h: 66 }
        ]);

        Turtle.animations = new TurtleAnimations();

        Turtle.animations.walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk0", delay: 100 },
            { name: "walk1", delay: 100 },
            { name: "walk2", delay: 100 },
            { name: "walk3", delay: 100 },
            { name: "walk4", delay: 100 },
            { name: "walk5", delay: 100 },
            { name: "walk6", delay: 100 },
            { name: "walk7", delay: 100 }
        ]);

        Turtle.animations.dive = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "dive0", delay: 1000 },
            { name: "dive1", delay: 1000 },
            { name: "dive2", delay: 1000 }
        ]);

        Turtle.animations.stuck = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "stuck0", delay: 200 },
            { name: "stuck1", delay: 200 },
            { name: "stuck2", delay: 200 },
            { name: "stuck3", delay: 200 }
        ]);
    }

    static Unload() {
        Turtle.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.TURTLE);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 17, 14, 43, 31);
        Playnewton.PPU.SetBodyPosition(this.body, x, y - 66);
        Playnewton.PPU.SetBodyImmovable(this.body, true);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.walkSpeed, this.walkSpeed, -this.walkSpeed, this.walkSpeed);
        Playnewton.PPU.EnableBody(this.body);

        this.state = new TurtleStuckState(this);
    }

    UpdateBody() {
        this.state.UpdateBody();
    }

    UpdateSprite() {
        this.state.UpdateSprite();
    }
}
