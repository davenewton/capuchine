import * as Playnewton from "../playnewton.js"
import { Portrait, PortraitAnimations } from "./dialog.js";

export default class TurtlePortrait extends Portrait {
    /**
     * @type PortraitAnimations
     */
     static portraitAnimations;

    constructor() {
        super(TurtlePortrait.portraitAnimations, 40, Playnewton.GPU.screenHeight - 128);
    }

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "talk0", x: 317, y: 40, w: 38, h: 38 },
            { name: "talk1", x: 356, y: 40, w: 38, h: 38 },
            { name: "talk2", x: 395, y: 40, w: 38, h: 38 },
            { name: "listen0", x: 434, y: 40, w: 38, h: 38 },
            { name: "listen1", x: 473, y: 40, w: 38, h: 38 }
        ]);

        TurtlePortrait.portraitAnimations = new PortraitAnimations();

        TurtlePortrait.portraitAnimations.listen = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "listen0", delay: 1000 },
            { name: "listen1", delay: 150 },
            { name: "listen0", delay: 150 },
            { name: "listen1", delay: 150 },
        ]);

        TurtlePortrait.portraitAnimations.talk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "talk0", delay: 100 },
            { name: "talk1", delay: 100 },
            { name: "talk2", delay: 100 },
        ]);
    }

    static Unload() {
        TurtlePortrait.portraitAnimations = null;
    }
}
