import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import { EntityColor } from "./entity_color.js";

export default class Ring {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type EntityColor
     */
    color;

    /**
     * @type Array<Playnewton.GPU_SpriteAnimation>
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/sky/sky_objects.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "red0", x: 1, y: 1, w: 64, h: 64 },
            { name: "red1", x: 66, y: 1, w: 64, h: 64 },
            { name: "red2", x: 131, y: 1, w: 64, h: 64 },
            { name: "red3", x: 196, y: 1, w: 64, h: 64 },
            { name: "red4", x: 261, y: 1, w: 64, h: 64 },
            { name: "red5", x: 326, y: 1, w: 64, h: 64 },
            { name: "red6", x: 391, y: 1, w: 64, h: 64 },
            { name: "red7", x: 1, y: 66, w: 64, h: 64 },

            { name: "blue0", x: 66, y: 66, w: 64, h: 64 },
            { name: "blue1", x: 131, y: 66, w: 64, h: 64 },
            { name: "blue2", x: 196, y: 66, w: 64, h: 64 },
            { name: "blue3", x: 261, y: 66, w: 64, h: 64 },
            { name: "blue4", x: 326, y: 66, w: 64, h: 64 },
            { name: "blue5", x: 391, y: 66, w: 64, h: 64 },
            { name: "blue6", x: 1, y: 131, w: 64, h: 64 },
            { name: "blue7", x: 66, y: 131, w: 64, h: 64 },


            { name: "green0", x: 131, y: 131, w: 64, h: 64 },
            { name: "green1", x: 196, y: 131, w: 64, h: 64 },
            { name: "green2", x: 261, y: 131, w: 64, h: 64 },
            { name: "green3", x: 326, y: 131, w: 64, h: 64 },
            { name: "green4", x: 391, y: 131, w: 64, h: 64 },
            { name: "green5", x: 1, y: 196, w: 64, h: 64 },
            { name: "green6", x: 66, y: 196, w: 64, h: 64 },
            { name: "green7", x: 131, y: 196, w: 64, h: 64 },

            { name: "yellow0", x: 196, y: 196, w: 64, h: 64 },
            { name: "yellow1", x: 261, y: 196, w: 64, h: 64 },
            { name: "yellow2", x: 326, y: 196, w: 64, h: 64 },
            { name: "yellow3", x: 391, y: 196, w: 64, h: 64 },
            { name: "yellow4", x: 1, y: 261, w: 64, h: 64 },
            { name: "yellow5", x: 66, y: 261, w: 64, h: 64 },
            { name: "yellow6", x: 131, y: 261, w: 64, h: 64 },
            { name: "yellow7", x: 196, y: 261, w: 64, h: 64 }
        ]);

        Ring.animations = [];

        Ring.animations[EntityColor.RED] = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "red0", delay: 100 },
            { name: "red1", delay: 100 },
            { name: "red2", delay: 100 },
            { name: "red3", delay: 100 },
            { name: "red4", delay: 100 },
            { name: "red5", delay: 100 },
            { name: "red6", delay: 100 },
            { name: "red7", delay: 100 }        
        ]);

        Ring.animations[EntityColor.GREEN] = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "green0", delay: 100 },
            { name: "green1", delay: 100 },
            { name: "green2", delay: 100 },
            { name: "green3", delay: 100 },
            { name: "green4", delay: 100 },
            { name: "green5", delay: 100 },
            { name: "green6", delay: 100 },
            { name: "green7", delay: 100 }        
        ]);

        Ring.animations[EntityColor.BLUE] = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "blue0", delay: 100 },
            { name: "blue1", delay: 100 },
            { name: "blue2", delay: 100 },
            { name: "blue3", delay: 100 },
            { name: "blue4", delay: 100 },
            { name: "blue5", delay: 100 },
            { name: "blue6", delay: 100 },
            { name: "blue7", delay: 100 }        
        ]);

        Ring.animations[EntityColor.YELLOW] = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "yellow0", delay: 100 },
            { name: "yellow1", delay: 100 },
            { name: "yellow2", delay: 100 },
            { name: "yellow3", delay: 100 },
            { name: "yellow4", delay: 100 },
            { name: "yellow5", delay: 100 },
            { name: "yellow6", delay: 100 },
            { name: "yellow7", delay: 100 }        
        ]);
    }

    static Unload() {
        Ring.animations = null;
    }

    /**
     * 
     * @param {EntityColor} color
     * @param {number} x 
     * @param {number} y 
     */
    constructor(color, x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        this.color = color;
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Ring.animations[color]);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.RINGS);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y - 64);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 8, 8, 56, 56);
        Playnewton.PPU.SetBodyPosition(this.body, x, y - 64);
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, 0, 0, 0, 0);
        Playnewton.PPU.EnableBody(this.body);
    }

}
