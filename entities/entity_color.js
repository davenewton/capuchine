/**
 * @readonly
 * @enum {number}
 */
 export const EntityColor = {
    RED: 0,
    GREEN: 1,
    BLUE: 2,
    YELLOW: 3
};