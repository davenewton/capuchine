import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

export class PortraitAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    talk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    listen;
}

export class Portrait {
    /**
     * @type PortraitAnimations
     */
    animations;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @param {PortraitAnimations} animations
     * @param {number} x 
     * @param {number} y
     */
    constructor(animations, x, y) {
        this.animations = animations;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations.listen);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y);
        Playnewton.GPU.EnableSprite(this.sprite);
        Playnewton.GPU.HUD.UseSpriteForHUD(this.sprite);
    }
}

export class DialogLine {
    /**
     * @type String
     */
    text;

    /**
     * @type string
     */
    color;

    /**
     * Speed in millisecond per chararcter
     * @type number
     */
    speed;

    /**
     * Delay before showing next line
     * @type number
     */
    delay;

    /**
     * @type CanvasTextAlign
     */
    align;

    /**
     * @type Portrait
     */
    portrait;

    /**
     * @type number
     */
    x;

    /**
     * @type number
     */
    y;

    /**
     * @type DOMHighResTimeStamp
     */
    startTime;

    /**
     * @type DOMHighResTimeStamp
     */
    endTime;

    /**
     * @type string
     */
    get currentText() {
        let text = this.text;
        let nbCharacter = (Playnewton.CLOCK.now - this.startTime) / this.speed;
        if (nbCharacter <= 0) {
            return;
        }
        if (nbCharacter < text.length) {
            text = text.slice(0, nbCharacter);
        }
        return text;
    }
}

export class Dialog {

    /**
     * @type boolean
     */
    skipped ;

    /**
     * @type boolean
     */
    keepLastLine = false;

    /**
     * @type Array<DialogLine>
     */
    lines = [];

    /**
     * @type Set<Portrait>
     */
    portraits = new Set();

    /**
     * @typedef DialogLineDescription
     * @type {object}
     * @property {string} text
     * @property {string}  [color]
     * @property {number} [delay]
     * @property {number} [speed]
     * @property {number} [x]
     * @property {number} [y]
     * @property {CanvasTextAlign} [align]
     * @property {Portrait} [portrait]
     */

    /**
     * @param {Array<DialogLineDescription>} lines
     */
    Start(lines) {
        this.skipped = false;
        let startTime = Playnewton.CLOCK.now;
        this.portraits.clear();
        this.lines = lines.map(desc => {
            let line = new DialogLine();
            line.text = desc.text;
            line.color = desc.color || "#ffffff";
            line.delay = desc.delay || 2000;
            line.speed = desc.speed || 50;
            line.align = desc.align || "left";
            line.portrait = desc.portrait;
            if(line.portrait) {
                this.portraits.add(line.portrait);
            }
            line.x = desc.x;
            line.y = desc.y;
            line.startTime = startTime;
            line.endTime = startTime + line.text.length * line.speed + line.delay;
            startTime = line.endTime;
            return line;
        });
    }

    Skip() {
        this.skipped = true;
    }

    /**
     * @param {Playnewton.GPU_Label} label 
     */
    Update(label) {
        let line = this.currentLine;
        if (line) {
            for(let portrait of this.portraits) {
                Playnewton.GPU.EnableSprite(portrait.sprite);
                Playnewton.GPU.SetSpriteAnimation(portrait.sprite, line.portrait === portrait ? portrait.animations.talk : portrait.animations.listen);
            }
            Playnewton.GPU.HUD.SetLabelAlign(label, line.align);
            Playnewton.GPU.HUD.SetLabelColor(label, line.color);
            Playnewton.GPU.HUD.SetLabelPosition(label, Number.isFinite(line.x) ? line.x : label.x, Number.isFinite(line.y) ? line.y : label.y);
            Playnewton.GPU.HUD.SetLabelText(label, line.currentText);
        } else {
            for(let portrait of this.portraits) {
                Playnewton.GPU.DisableSprite(portrait.sprite);
            }
            Playnewton.GPU.HUD.SetLabelText(label, "");
        }
    }

    /**
     * @type boolean
     */
    get done() {
        if(this.skipped) {
            return true;
        }
        if (this.lines.length > 0) {
            return this.lines[this.lines.length - 1].endTime < Playnewton.CLOCK.now;
        } else {
            return false;
        }
    }

    get doneOrNotStarted() {
        return this.lines.length === 0 || this.done;
    }

    /**
     * @returns DialogLine
     */
    get currentLine() {
        if (!this.done) {
            for (let i = this.lines.length - 1; i >= 0; --i) {
                let line = this.lines[i];
                if (Playnewton.CLOCK.now >= line.startTime) {
                    return line;
                }
            }
        }
        if(this.keepLastLine && this.lines.length > 0) {
            return this.lines[this.lines.length - 1];
        } else {
            return null;
        }
    }
}
