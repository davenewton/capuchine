import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";


/**
 * 
 * @type JellyfishAnimations
 */
class JellyfishAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    swim;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    attack;
}

export default class Jellyfish {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type boolean
     */
    attack = false;

    /**
     * @type JellyfishAnimations
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/sea/sea.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "swim0", x: 161, y: 0, w: 47, h: 42 },
            { name: "swim1", x: 161, y: 42, w: 47, h: 42 },
            { name: "swim2", x: 161, y: 85, w: 47, h: 42 },
            { name: "swim3", x: 161, y: 128, w: 47, h: 42 },
            { name: "swim4", x: 161, y: 172, w: 47, h: 42 },

            { name: "attack0", x: 209, y: 0, w: 47, h: 42 },
            { name: "attack1", x: 209, y: 42, w: 47, h: 42 },
            { name: "attack2", x: 209, y: 85, w: 47, h: 42 },
            { name: "attack3", x: 209, y: 128, w: 47, h: 42 },
            { name: "attack4", x: 209, y: 172, w: 47, h: 42 },
        ]);

        Jellyfish.animations = new JellyfishAnimations();

        Jellyfish.animations.swim = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "swim0", delay: 150 },
            { name: "swim1", delay: 150 },
            { name: "swim2", delay: 150 },
            { name: "swim3", delay: 150 },
            { name: "swim4", delay: 150 },
            { name: "swim3", delay: 150 },
            { name: "swim2", delay: 150 },
            { name: "swim1", delay: 150 }
        ]);

        Jellyfish.animations.attack = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "attack0", delay: 100 },
            { name: "attack1", delay: 100 },
            { name: "attack2", delay: 100 },
            { name: "attack3", delay: 100 },
            { name: "attack4", delay: 100 },
            { name: "attack3", delay: 100 },
            { name: "attack2", delay: 100 },
            { name: "attack1", delay: 100 }
        ]);
    }

    static Unload() {
        Jellyfish.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Jellyfish.animations.swim);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.JELLYFISH);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 12, 9, 19, 24);
        Playnewton.PPU.SetBodyPosition(this.body, x, y - 42);
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, 0, 0, 0, 0);
        Playnewton.PPU.EnableBody(this.body);
    }

    UpdateBody() {
        //TODO
    }

    UpdateSprite() {
        if (this.attack) {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, Jellyfish.animations.attack);
        } else {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, Jellyfish.animations.swim);
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
    }
}
