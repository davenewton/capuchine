import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import { EntityColor } from "./entity_color.js";

const FLY_SPEED = 2;
const CATCHUP_SPEED = 3;

const MOVE_SPEED = 1;

const DISTANCE_BETWEEN_PARTS = 24;

/**
 * 
 * @type DragonPartAnimations
 */
class DragonPartAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    fly;
}

/**
 * 
 * @type DragonAnimations
 */
class DragonAnimations {
    /**
     * @type  Array<DragonPartAnimations>
     */
    head = [];
    /**
     * @type  Array<DragonPartAnimations>
     */
    body = [];
    /**
     * @type  Array<DragonPartAnimations>
     */
    bodyWithCapuchine = [];
    /**
     * @type  Array<DragonPartAnimations>
     */
    tail = [];
}

class DragonPart {

    /**
     * @type DragonPart
     */
    previousPart;

    /**
     * @type Array<DragonPartAnimations>
     */
    animations = [];

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type EntityColor
     */
    color = EntityColor.RED;

    UpdateSprite() {
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations[this.color].fly);
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
    }

    UpdateBody() {
        if (this.previousPart) {
            let dx = this.previousPart.body.centerX - this.body.centerX;
            let dy = this.previousPart.body.centerY - this.body.centerY;
            let distance = Math.sqrt(dx ** 2 + dy ** 2);
            if (distance > DISTANCE_BETWEEN_PARTS) {
                let s = CATCHUP_SPEED / distance;
                dx *= s;
                dy *= s;
            } else {
                dx = 0;
                dy = 0;
            }
            Playnewton.PPU.SetBodyVelocity(this.body, dx, dy);
        }
    }
}

class DragonHead extends DragonPart {

    /**
     * @type Array<Playnewton.PPU_Point>
     */
    destinations = [];

    /**
     * @type number
     */
    currentDestination = 0;

    /**
     * @type number
     */
    destinationIncrement = 1;

    /**
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        super();
        this.animations = Dragon.animations.head;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.DRAGON);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, true, true, false, false);
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 27, 18);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -FLY_SPEED, FLY_SPEED, -FLY_SPEED, FLY_SPEED);
        Playnewton.PPU.EnableBody(this.body);
    }

    UpdateBody() {
        let pad = Playnewton.CTRL.GetMasterPad();
        let velocityX = -FLY_SPEED;
        let velocityY = 0;
        if (pad.up) {
            velocityY -= MOVE_SPEED;
        } else if (pad.down) {
            velocityY += MOVE_SPEED;
        } else {
            velocityY = 0;
        }
        Playnewton.PPU.SetBodyVelocity(this.body, velocityX, velocityY);
    }

    UpdateSprite() {
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations[this.color].fly);
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - 5, this.body.position.y - 6);
    }
}

class DragonBody extends DragonPart {
    /**
     * @param {number} x
     * @param {number} y
     */
    constructor(x, y) {
        super();
        this.animations = Dragon.animations.body;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.DRAGON);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 4, 6, 20, 22);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -FLY_SPEED, FLY_SPEED, -FLY_SPEED, FLY_SPEED);
        Playnewton.PPU.EnableBody(this.body);
    }
}

class DragonBodyWithCapuchine extends DragonPart {
    /**
     * @param {number} x
     * @param {number} y
     */
    constructor(x, y) {
        super();
        this.animations = Dragon.animations.bodyWithCapuchine;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.DRAGON);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 4, 6, 20, 22);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -FLY_SPEED, FLY_SPEED, -FLY_SPEED, FLY_SPEED);
        Playnewton.PPU.EnableBody(this.body);
    }

    UpdateSprite() {
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations[this.color].fly);
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y - 30);
    }
}

class DragonTail extends DragonPart {
    /**
     * @param {number} x
     * @param {number} y
     */
    constructor(x, y) {
        super();
        this.health = Infinity;
        this.animations = Dragon.animations.tail;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.DRAGON);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 5, 6, 27, 18);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -FLY_SPEED, FLY_SPEED, -FLY_SPEED, FLY_SPEED);
        Playnewton.PPU.EnableBody(this.body);
    }
}

export default class Dragon {

    /**
     * @type Array<DragonPart>
     */
    parts = [];

    /**
     * @type DragonAnimations
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "head-red0", x: 1, y: 401, w: 53, h: 31 },
            { name: "head-red1", x: 55, y: 401, w: 53, h: 31 },
            { name: "head-red2", x: 108, y: 401, w: 53, h: 31 },
            { name: "body-capuchine-red0", x: 193 , y: 2, w: 30, h: 61 },
            { name: "body-red0", x: 161 , y: 401, w: 30, h: 30 },
            { name: "tail-red0", x: 192, y: 401, w: 49, h: 30 },

            { name: "head-green0", x: 1, y: 433, w: 53, h: 31 },
            { name: "head-green1", x: 55, y: 433, w: 53, h: 31 },
            { name: "head-green2", x: 108, y: 433, w: 53, h: 31 },
            { name: "body-capuchine-green0", x: 225 , y: 2, w: 30, h: 61 },
            { name: "body-green0", x: 161 , y: 433, w: 30, h: 30 },
            { name: "tail-green0", x: 192, y: 433, w: 49, h: 30 },

            { name: "head-blue0", x: 1, y: 465, w: 53, h: 31 },
            { name: "head-blue1", x: 55, y: 465, w: 53, h: 31 },
            { name: "head-blue2", x: 108, y: 465, w: 53, h: 31 },
            { name: "body-capuchine-blue0", x: 257 , y: 2, w: 30, h: 61 },
            { name: "body-blue0", x: 161 , y: 465, w: 30, h: 30 },
            { name: "tail-blue0", x: 192, y: 465, w: 49, h: 30 },

            { name: "head-yellow0", x: 242, y: 465, w: 53, h: 31 },
            { name: "head-yellow1", x: 296, y: 465, w: 53, h: 31 },
            { name: "head-yellow2", x: 349, y: 465, w: 53, h: 31 },
            { name: "body-capuchine-yellow0", x: 193 , y: 66, w: 30, h: 61 },
            { name: "body-yellow0", x: 402 , y: 465, w: 30, h: 30 },
            { name: "tail-yellow0", x: 433, y: 465, w: 49, h: 30 }

        ]);

        Dragon.animations = new DragonAnimations();

        for(let colorEntry of Object.entries(EntityColor)) {
            let colorName = colorEntry[0].toLowerCase();
            let colorValue = colorEntry[1];

            Dragon.animations.head[colorValue] =  new DragonPartAnimations();
            Dragon.animations.head[colorValue].fly = Playnewton.GPU.CreateAnimation(spriteset, [
                { name: `head-${colorName}0`, delay: 100 },
                { name: `head-${colorName}1`, delay: 100 },
                { name: `head-${colorName}2`, delay: 100 }
            ]);
    
            Dragon.animations.bodyWithCapuchine[colorValue] =  new DragonPartAnimations();
            Dragon.animations.bodyWithCapuchine[colorValue].fly = Playnewton.GPU.CreateAnimation(spriteset, [
                { name: `body-capuchine-${colorName}0`, delay: 100 }
            ]);
    
            Dragon.animations.body[colorValue] =  new DragonPartAnimations();
            Dragon.animations.body[colorValue].fly = Playnewton.GPU.CreateAnimation(spriteset, [
                { name: `body-${colorName}0`, delay: 100 }
            ]);
    
            Dragon.animations.tail[colorValue] =  new DragonPartAnimations();
            Dragon.animations.tail[colorValue].fly = Playnewton.GPU.CreateAnimation(spriteset, [
                { name: `tail-${colorName}0`, delay: 100 }
            ]);
        }
    }

    static Unload() {
        Dragon.animations = null;
    }

    constructor(x, y) {
        const nbParts = 8;
        this.parts.unshift(new DragonTail(x + nbParts * DISTANCE_BETWEEN_PARTS, y));
        for (let i = nbParts - 1; i >= 2; i--) {
            this.parts.unshift(new DragonBody(x + i * DISTANCE_BETWEEN_PARTS, y));
        }
        this.parts.unshift(new DragonBodyWithCapuchine(x + DISTANCE_BETWEEN_PARTS, y));
        this.parts.unshift(new DragonHead(x, y));
        for (let i = 1; i < this.parts.length; i++) {
            this.parts[i].previousPart = this.parts[i - 1];
        }
    }

    UpdateSprite() {
        for (let p of this.parts) {
            p.UpdateSprite();
        }
    }

    UpdateBody() {
        for (let p of this.parts) {
            p.UpdateBody();
        }
    }
}