import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

/**
 * 
 * @type CapuchineOnTurtleAnimations
 */
class CapuchineOnTurtleAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    stand;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    swim;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shocked;
}

export default class CapuchineOnTurtle {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     *  @type number
     */
    swimSpeed = 2;

    /**
     * @type boolean
     */
    shock = false;

    /**
     * @type CapuchineOnTurtleAnimations
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "swim0", x: 366, y: 197, w: 70, h: 61 },
            { name: "swim1", x: 366, y: 265, w: 70, h: 61 },
            { name: "swim2", x: 366, y: 333, w: 70, h: 61 },
            { name: "swim3", x: 439, y: 197, w: 70, h: 61 },
            { name: "swim4", x: 439, y: 265, w: 70, h: 61 },
            { name: "stand0", x: 366, y: 265, w: 70, h: 61 },
            { name: "shocked0", x: 366, y: 401, w: 70, h: 61 },
            { name: "shocked1", x: 439, y: 333, w: 70, h: 61 },
            { name: "shocked2", x: 439, y: 401, w: 70, h: 61 },
        ]);

        CapuchineOnTurtle.animations = new CapuchineOnTurtleAnimations();

        CapuchineOnTurtle.animations.swim = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "swim0", delay: 200 },
            { name: "swim1", delay: 200 },
            { name: "swim2", delay: 200 },
            { name: "swim3", delay: 200 }
        ]);

        CapuchineOnTurtle.animations.stand = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "stand0", delay: 1000 }
        ]);

        CapuchineOnTurtle.animations.shocked = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shocked0", delay: 100 },
            { name: "shocked1", delay: 100 },
            { name: "shocked2", delay: 100 }
        ]);
    }

    static Unload() {
        CapuchineOnTurtle.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, CapuchineOnTurtle.animations.stand);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.CAPUCHINE);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 32, 48);
        Playnewton.PPU.SetBodyPosition(this.body, x, y - 48);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, true, true, true, true);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.swimSpeed, this.swimSpeed, -this.swimSpeed, this.swimSpeed);
        Playnewton.PPU.EnableBody(this.body);
    }

    UpdateBody() {
        let pad = Playnewton.CTRL.GetMasterPad();
        let velocityX = this.body.velocity.x;
        let velocityY = this.body.velocity.y;

        if (pad.up) {
            velocityY -= this.swimSpeed;
        } else if (pad.down) {
            velocityY += this.swimSpeed;
        } else {
            velocityY = 0;
        }

        if (pad.left) {
            velocityX -= this.swimSpeed;
        } else if (pad.right) {
            velocityX += this.swimSpeed;
        } else {
            velocityX = 0;
        }

        Playnewton.PPU.SetBodyVelocity(this.body, velocityX, velocityY);
    }

    UpdateSprite() {
        if(this.shock) {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, CapuchineOnTurtle.animations.shocked);
        } else if (this.body.velocity.length < Number.EPSILON) {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, CapuchineOnTurtle.animations.stand);
        } else {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, CapuchineOnTurtle.animations.swim);
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
    }
}
