import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

/**
 * @readonly
 * @enum {number}
 */
const CapuchineDirection = {
    UP: 0,
    DOWN: 1,
    LEFT: 2,
    RIGHT: 3
};

/**
 * 
 * @type CapuchineAnimations
 */
class CapuchineAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    stand;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    walk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    push;
}

export default class Capuchine {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     *  @type number
     */
    walkSpeed = 2;

    /**
     * @type CapuchineDirection
     */
    direction = CapuchineDirection.DOWN;

    /**
     * @type CapuchineAnimations[]
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "walk-down0", x: 1, y: 1, w: 30, h: 45 },
            { name: "walk-down1", x: 33, y: 1, w: 30, h: 45 },
            { name: "walk-down2", x: 65, y: 1, w: 30, h: 45 },
            { name: "walk-up0", x: 1, y: 49, w: 30, h: 45 },
            { name: "walk-up1", x: 33, y: 49, w: 30, h: 45 },
            { name: "walk-up2", x: 65, y: 49, w: 30, h: 45 },
            { name: "walk-left0", x: 1, y: 97, w: 30, h: 45 },
            { name: "walk-left1", x: 33, y: 97, w: 30, h: 45 },
            { name: "walk-left2", x: 65, y: 97, w: 30, h: 45 },
            { name: "walk-right0", x: 1, y: 145, w: 30, h: 45 },
            { name: "walk-right1", x: 33, y: 145, w: 30, h: 45 },
            { name: "walk-right2", x: 65, y: 145, w: 30, h: 45 },

            { name: "push-down0", x: 97, y: 1, w: 30, h: 45 },
            { name: "push-down1", x: 129, y: 1, w: 30, h: 45 },
            { name: "push-down2", x: 161, y: 1, w: 30, h: 45 },
            { name: "push-up0", x: 97, y: 49, w: 30, h: 45 },
            { name: "push-up1", x: 129, y: 49, w: 30, h: 45 },
            { name: "push-up2", x: 161, y: 49, w: 30, h: 45 },
            { name: "push-left0", x: 97, y: 97, w: 30, h: 45 },
            { name: "push-left1", x: 129, y: 97, w: 30, h: 45 },
            { name: "push-left2", x: 161, y: 97, w: 30, h: 45 },
            { name: "push-right0", x: 97, y: 145, w: 30, h: 45 },
            { name: "push-right1", x: 129, y: 145, w: 30, h: 45 },
            { name: "push-right2", x: 161, y: 145, w: 30, h: 45 },
        ]);

        Capuchine.animations = [];

        Capuchine.animations[CapuchineDirection.UP] = new CapuchineAnimations();
        Capuchine.animations[CapuchineDirection.DOWN] = new CapuchineAnimations();
        Capuchine.animations[CapuchineDirection.LEFT] = new CapuchineAnimations();
        Capuchine.animations[CapuchineDirection.RIGHT] = new CapuchineAnimations();

        Capuchine.animations[CapuchineDirection.UP].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-up0", delay: 100 },
            { name: "walk-up1", delay: 100 },
            { name: "walk-up0", delay: 100 },
            { name: "walk-up2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.DOWN].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-down0", delay: 100 },
            { name: "walk-down1", delay: 100 },
            { name: "walk-down0", delay: 100 },
            { name: "walk-down2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.LEFT].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-left0", delay: 100 },
            { name: "walk-left1", delay: 100 },
            { name: "walk-left0", delay: 100 },
            { name: "walk-left2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.RIGHT].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-right0", delay: 100 },
            { name: "walk-right1", delay: 100 },
            { name: "walk-right0", delay: 100 },
            { name: "walk-right2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.UP].push = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "push-up0", delay: 100 },
            { name: "push-up1", delay: 100 },
            { name: "push-up0", delay: 100 },
            { name: "push-up2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.DOWN].push = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "push-down0", delay: 100 },
            { name: "push-down1", delay: 100 },
            { name: "push-down0", delay: 100 },
            { name: "push-down2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.LEFT].push = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "push-left0", delay: 100 },
            { name: "push-left1", delay: 100 },
            { name: "push-left0", delay: 100 },
            { name: "push-left2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.RIGHT].push = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "push-right0", delay: 100 },
            { name: "push-right1", delay: 100 },
            { name: "push-right0", delay: 100 },
            { name: "push-right2", delay: 100 }
        ]);

        Capuchine.animations[CapuchineDirection.UP].stand = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-up0", delay: 1000 }
        ]);

        Capuchine.animations[CapuchineDirection.DOWN].stand = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-down0", delay: 1000 }
        ]);

        Capuchine.animations[CapuchineDirection.LEFT].stand = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-left0", delay: 1000 }
        ]);

        Capuchine.animations[CapuchineDirection.RIGHT].stand = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-right0", delay: 1000 }
        ]);
    }

    static Unload() {
        Capuchine.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Capuchine.animations[CapuchineDirection.LEFT].stand);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.CAPUCHINE);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 32, 48);
        Playnewton.PPU.SetBodyPosition(this.body, x, y - 48);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, true, true, true, true);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.walkSpeed, this.walkSpeed, -this.walkSpeed, this.walkSpeed);
        Playnewton.PPU.EnableBody(this.body);
    }

    UpdateBody() {
        let pad = Playnewton.CTRL.GetMasterPad();
        let velocityX = this.body.velocity.x;
        let velocityY = this.body.velocity.y;

        if (pad.up) {
            this.direction = CapuchineDirection.UP;
            velocityY -= this.walkSpeed;
        } else if (pad.down) {
            this.direction = CapuchineDirection.DOWN;
            velocityY += this.walkSpeed;
        } else {
            velocityY = 0;
        }

        if (pad.left) {
            this.direction = CapuchineDirection.LEFT;
            velocityX -= this.walkSpeed;
        } else if (pad.right) {
            this.direction = CapuchineDirection.RIGHT;
            velocityX += this.walkSpeed;
        } else {
            velocityX = 0;
        }

        Playnewton.PPU.SetBodyVelocity(this.body, velocityX, velocityY);
    }

    UpdateSprite() {
        if (this.body.velocity.length < Number.EPSILON) {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, Capuchine.animations[this.direction].stand);
        } else {
            if (this.body.touches.bottom && this.direction === CapuchineDirection.DOWN ||
                this.body.touches.top && this.direction === CapuchineDirection.UP ||
                this.body.touches.left && this.direction === CapuchineDirection.LEFT ||
                this.body.touches.right && this.direction === CapuchineDirection.RIGHT
            ) {
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Capuchine.animations[this.direction].push);
            } else {
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Capuchine.animations[this.direction].walk);
            }
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
    }
}
