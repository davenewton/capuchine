import * as Playnewton from "../playnewton.js"
import { Portrait, PortraitAnimations } from "./dialog.js";

export default class CapuchinePortrait extends Portrait {
    /**
     * @type PortraitAnimations
     */
     static portraitAnimations;

    constructor() {
        super(CapuchinePortrait.portraitAnimations, Playnewton.GPU.screenWidth - 80, Playnewton.GPU.screenHeight - 128);
    }

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/capuchine.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "talk0", x: 317, y: 1, w: 38, h: 38 },
            { name: "talk1", x: 356, y: 1, w: 38, h: 38 },
            { name: "talk2", x: 395, y: 1, w: 38, h: 38 },
            { name: "listen0", x: 434, y: 1, w: 38, h: 38 },
            { name: "listen1", x: 473, y: 1, w: 38, h: 38 }
        ]);

        CapuchinePortrait.portraitAnimations = new PortraitAnimations();

        CapuchinePortrait.portraitAnimations.listen = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "listen0", delay: 1000 },
            { name: "listen1", delay: 100 },
            { name: "listen0", delay: 100 },
            { name: "listen1", delay: 100 }
        ]);

        CapuchinePortrait.portraitAnimations.talk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "talk0", delay: 150 },
            { name: "talk1", delay: 150 },
            { name: "talk2", delay: 150 }
        ]);
    }

    static Unload() {
        CapuchinePortrait.portraitAnimations = null;
    }
}
