const CACHE_NAME = 'capuchine-v1';

/**
 * @type ExtendableEvent
 */
self.addEventListener('install', (event) => {
    console.log(`Install cache ${CACHE_NAME}`);
    let precache = async () => {
        let cache = await caches.open(CACHE_NAME);
        return cache.addAll([
            '/',
            "capuchine_game.js",
            "entities/announcement.js",
            "entities/capuchine.js",
            "entities/capucine_on_turtle.js",
            "entities/capucine_portrait.js",
            "entities/dialog.js",
            "entities/dragon_portrait.js",
            "entities/dragon.js",
            "entities/entity_color.js",
            "entities/fadeout.js",
            "entities/jellyfish.js",
            "entities/ring.js",
            "entities/turtle_portrait.js",
            "entities/turtle.js",
            "index.css",
            "index.html",
            "maps/beach/beach.png",
            "maps/beach/beach.tmx",
            "maps/beach/beach.tsx",
            "maps/icons/capuchine_on_dragon-icon.png",
            "maps/icons/capuchine_on_turtle-icon.png",
            "maps/icons/capuchine-icon.png",
            "maps/icons/icons.tsx",
            "maps/icons/jellyfish-icon.png",
            "maps/icons/turtle-icon.png",
            "maps/sea/palace_door.png",
            "maps/sea/palace.png",
            "maps/sea/sea_background.png",
            "maps/sea/sea_bg.tsx",
            "maps/sea/sea.png",
            "maps/sea/sea.tmx",
            "maps/sea/sea.tsx",
            "maps/sky/sky_bg.tsx",
            "maps/sky/sky_objects.png",
            "maps/sky/sky.png",
            "maps/sky/sky.tmx",
            "maps/sky/sky.tsx",
            "playnewton.js",
            "scenes/beach_level.js",
            "scenes/ending_scene.js",
            "scenes/scene.js",
            "scenes/sea_level.js",
            "scenes/sky_level.js",
            "scenes/title.js",
            "serviceWorker.js",
            "sounds/hurt-capuchine.wav",
            "sounds/menu-select.wav",
            "sounds/pause_resume.wav",
            "sounds/skip.wav",
            "sprites/capuchine.png",
            "sprites/ending.png",
            "sprites/title.png",
            "utils/keyboard_mappings.js",
            "utils/z_order.js"
         ]);
    }
    event.waitUntil(precache());
});

/**
 * @type FetchEvent
 */
self.addEventListener('fetch', (event) => {
    let respond = async () => {
        let cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(event.request);
        if (cachedResponse) {
            return cachedResponse;
        } else {
            return fetch(event.request);
        }
    }
    event.respondWith(respond());
});

self.addEventListener('activate', function (event) {
    let updateCache = async () => {
        let keys = await caches.keys();
        return Promise.all(keys.map((key) => {
            if (key !== CACHE_NAME) {
                console.log(`Delete cache ${key}`);
                return caches.delete(key);
            }
        }));
    }
    event.waitUntil(updateCache());
});
