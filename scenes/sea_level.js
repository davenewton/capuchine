import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import Fadeout from "../entities/fadeout.js"
import { Dialog } from "../entities/dialog.js"
import CapuchinePortrait from "../entities/capucine_portrait.js"
import TurtlePortrait from "../entities/turtle_portrait.js"
import CapuchineOnTurtle from "../entities/capucine_on_turtle.js"
import Jellyfish from "../entities/jellyfish.js"
import DragonPortrait from "../entities/dragon_portrait.js"

class SeaLevelState {
    /**
     * @type SeaLevel
     */
    level;

    /**
     * @param {SeaLevel} level 
     */
    constructor(level) {
        this.level = level;
    }

    UpdateBodies() {
        this.level.capuchineAndTurtle.shock = false;
        for(let jellyfish of this.level.jellyfishes) {
            jellyfish.UpdateBody();
            if(Playnewton.PPU.CheckIfBodiesIntersects(jellyfish.body, this.level.capuchineAndTurtle.body)) {
                jellyfish.attack = true;
                this.level.capuchineAndTurtle.shock = true;
            } else {
                jellyfish.attack = false;
            }
        }
        this.level.capuchineAndTurtle.UpdateBody();
    }

    UpdateSprites() {
        let scrollX = Playnewton.FPU.bound(-5120 + 1024, -this.level.capuchineAndTurtle.sprite.x + 512, 0);
        Playnewton.GPU.SetScroll(scrollX, 0);

        this.level?.levelAnnouncement.Update();

        for(let jellyfish of this.level.jellyfishes) {
            jellyfish.UpdateSprite();
        }
        this.level.capuchineAndTurtle.UpdateSprite();
    }
}

class SeaLevelSwimState extends SeaLevelState {
    
    /**
     * @param {SeaLevel} level 
     */
     constructor(level) {
        super(level);
        this.level.levelAnnouncement = new Announcement("Sea");
        this.level.levelAnnouncement.Start();
    }

    UpdateSprites() {
        super.UpdateSprites();
        if(this.level.capuchineAndTurtle.body.right > 4600) {
            this.level.state = new SeaLevelDialogState(this.level);
        }
    }
}

class SeaLevelDialogState extends SeaLevelState {

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Dialog
     */
    dialog;
    
    /**
     * @param {SeaLevel} level 
     */
     constructor(level) {
        super(level);

        let capuchinePortrait = new CapuchinePortrait();
        let turtlePortrait = new TurtlePortrait();
        let dragonPortrait = new DragonPortrait();

        this.dialog = new Dialog();

        this.dialog.Start([
            { color: "#adb834", text: "[Turtle] Hi Dragon, Capuchine helped me !", portrait: turtlePortrait, delay: 5000 },
            { color: "#bc4a9b", text: "[Capuchine] She was stuck on her back.", portrait: capuchinePortrait, delay: 5000 },
            { color: "#df3e23", text: "[Dragon] Thank you Capuchine.", portrait: dragonPortrait, delay: 5000},
            { color: "#df3e23", text: "[Dragon] Do you want to fly with me?", portrait: dragonPortrait, delay: 5000},
            { color: "#bc4a9b", text: "[Capuchine] I'd love to fly !", portrait: capuchinePortrait, delay: 5000 },
            { color: "#df3e23", text: "[Dragon] Get on my back !", portrait: dragonPortrait, delay: 5000 }
        ]);

        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
        Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
    }

    UpdateSprites() {
        super.UpdateSprites();
        let pad = Playnewton.CTRL.GetMasterPad();
        if (pad.TestStartAndResetIfPressed()) {
            Playnewton.APU.PlaySound("sounds/skip.wav");
            this.dialog.Skip();
        }
        this.dialog.Update(this.dialogLabel);
        if(this.dialog.done) {
            this.level.state = new SeaLevelFadeoutState(this.level);
        }
    }

}

class SeaLevelFadeoutState extends SeaLevelState {

    /**
     * @type Fadeout
     */
    fadeout;
    
    /**
     * @param {SeaLevel} level 
     */
     constructor(level) {
        super(level);
        let layers = [];
        for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
            layers.push(i);
        }
        this.fadeout = new Fadeout(1000, layers, () => {
            this.level.Stop();
            this.level.nextScene = this.level.nextSceneOnExit;
            this.level.nextSceneOnExit.Start();
        });
    }

    UpdateSprites() {
        super.UpdateSprites();
        this.fadeout.Update();
    }

}

export default class SeaLevel extends Scene {

    /**
     * @type SeaLevelState
     */
    state;

    /**
     * @type CapuchineOnTurtle
     */
    capuchineAndTurtle;

    /**
     * @type Array<Jellyfish>
     */
    jellyfishes;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @param {Scene} nextSceneOnExit 
     */
    constructor(nextSceneOnExit) {
        super();
        this.pausable = false;
        this.nextSceneOnExit = nextSceneOnExit;       
    }

    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "jellyfish":
                        this.jellyfishes.push(new Jellyfish(x, y));
                        break;
                    case "capuchine_on_turtle":
                        if (!this.capuchineAndTurtle) {
                            this.capuchineAndTurtle = new CapuchineOnTurtle(x, y);
                        }
                        break;
                }
            },
            map);
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap("maps/sea/sea.tmx");

        Playnewton.PPU.SetWorldBounds(0, 0, 4700, 570);
        Playnewton.PPU.SetWorldGravity(0, 0.1);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    async InitDialog() {
        await CapuchinePortrait.Load();
        await TurtlePortrait.Load();
        await DragonPortrait.Load();
    }

    async Start() {
        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            //TODO
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 20;

        await CapuchineOnTurtle.Load();
        this.progress = 30;

        await Jellyfish.Load();
        this.jellyfishes = [];
        this.progress = 40;

        await this.InitDialog();
        this.progress = 60;

        await this.InitMap();
        this.progress = 80;

        await audioPromise;
        this.progress = 100;

        this.state = new SeaLevelSwimState(this);
    }

    Stop() {
        super.Stop();

        CapuchineOnTurtle.Unload();
        this.capuchineAndTurtle = null;

        Jellyfish.Unload();
        this.jellyfishes = [];

        CapuchinePortrait.Unload();
        TurtlePortrait.Unload();
        DragonPortrait.Unload();

        this.state = null;
    }

    UpdateBodies() {
        this.state?.UpdateBodies();
    }

    UpdateSprites() {
        this.state?.UpdateSprites();
    }
}
