import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Fadeout from "../entities/fadeout.js"
import { Dialog } from "../entities/dialog.js"
import CapuchinePortrait from "../entities/capucine_portrait.js"
import DragonPortrait from "../entities/dragon_portrait.js"

class EndingSceneState {
    /**
     * @type EndingScene
     */
    scene;

    /**
     * @param {EndingScene} scene 
     */
    constructor(scene) {
        this.scene = scene;
    }

    UpdateBodies() {
    }

    UpdateSprites() {
    }
}

class EndingSceneDialogState extends EndingSceneState {

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Dialog
     */
    dialog;
    
    /**
     * @param {EndingScene} Scene 
     */
     constructor(Scene) {
        super(Scene);

        let capuchinePortrait = new CapuchinePortrait();
        let dragonPortrait = new DragonPortrait();

        this.dialog = new Dialog();

        this.dialog.Start([
            { color: "#df3e23", text: "[Dragon] I drop you here ?", portrait: dragonPortrait, delay: 5000 },
            { color: "#bc4a9b", text: "[Capuchine] Yes, thank you Dragon !", portrait: capuchinePortrait, delay: 5000 },
            { color: "#df3e23", text: "[Dragon] Bye Capuchine !", portrait: dragonPortrait, delay: 5000},
            { color: "#bc4a9b", text: "[Capuchine] Let's go home.", portrait: capuchinePortrait, delay: 5000 },
            { color: "#ffffff", text: "THE END", speed: 200, align: "center", x: 512, y: 532, delay: 10000 }
        ]);

        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
        Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
    }

    UpdateSprites() {
        super.UpdateSprites();
        let pad = Playnewton.CTRL.GetMasterPad();
        if (pad.TestStartAndResetIfPressed()) {
            Playnewton.APU.PlaySound("sounds/skip.wav");
            this.dialog.Skip();
        }
        this.dialog.Update(this.dialogLabel);
        if(this.dialog.done) {
            this.scene.state = new EndingSceneFadeoutState(this.scene);
        }
    }

}

class EndingSceneFadeoutState extends EndingSceneState {

    /**
     * @type Fadeout
     */
    fadeout;
    
    /**
     * @param {EndingScene} Scene 
     */
     constructor(Scene) {
        super(Scene);
        let layers = [];
        for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
            layers.push(i);
        }
        this.fadeout = new Fadeout(1000, layers, () => {
            this.scene.Stop();
            this.scene.nextScene = this.scene.nextSceneOnExit;
            this.scene.nextSceneOnExit.Start();
        });
    }

    UpdateSprites() {
        super.UpdateSprites();
        this.fadeout.Update();
    }

}

export default class EndingScene extends Scene {

    /**
     * @type ImageBitmap
     */
    backgroundBitmap;

    /**
     * @type EndingSceneState
     */
    state;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @param {Scene} nextSceneOnExit 
     */
    constructor(nextSceneOnExit) {
        super();
        this.pausable = false;
        this.nextSceneOnExit = nextSceneOnExit;       
    }

    async InitWorld() {
        Playnewton.PPU.SetWorldBounds(0, 0, 1024, 576);

        this.backgroundBitmap = await Playnewton.DRIVE.LoadBitmap("sprites/ending.png");
        let backgroundPicture = Playnewton.GPU.CreatePicture(this.backgroundBitmap);

        let backgroundSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePicture(backgroundSprite, backgroundPicture);
        Playnewton.GPU.SetSpritePosition(backgroundSprite, 0, 0);
        Playnewton.GPU.EnableSprite(backgroundSprite);
    }

    async InitDialog() {
        await CapuchinePortrait.Load();
        await DragonPortrait.Load();
    }

    async Start() {
        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            //TODO
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 20;

        await this.InitDialog();
        this.progress = 40;

        await this.InitWorld();
        this.progress = 80;

        await audioPromise;
        this.progress = 100;

        this.state = new EndingSceneDialogState(this);
    }

    Stop() {
        super.Stop();

        CapuchinePortrait.Unload();
        DragonPortrait.Unload();

        this.backgroundBitmap = null;
        this.state = null;
    }

    UpdateBodies() {
        this.state?.UpdateBodies();
    }

    UpdateSprites() {
        this.state?.UpdateSprites();
    }
}
