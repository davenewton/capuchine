import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import Fadeout from "../entities/fadeout.js"
import CapuchinePortrait from "../entities/capucine_portrait.js"
import DragonPortrait from "../entities/dragon_portrait.js"
import Dragon from "../entities/dragon.js"
import Ring from "../entities/ring.js"
import { EntityColor } from "../entities/entity_color.js"
import EndingScene from "./ending_scene.js"

class SkyLevelState {
    /**
     * @type SkyLevel
     */
    level;

    /**
     * @param {SkyLevel} level 
     */
    constructor(level) {
        this.level = level;
    }

    UpdateBodies() {
        for (let part of this.level.dragon.parts) {
            for (let ring of this.level.rings) {
                if (Playnewton.PPU.CheckIfBodiesIntersects(ring.body, part.body)) {
                    part.color = ring.color;
                }
            }
        }
        this.level.dragon.UpdateBody();
    }

    UpdateSprites() {
        let scrollX = Playnewton.FPU.bound(-8192 + 1024, -this.level.dragon.parts[0].sprite.x + 512, 0);
        Playnewton.GPU.SetScroll(scrollX, 0);
        this.level.dragon.UpdateSprite();
    }
}

class SkyLevelFlyState extends SkyLevelState {

    /**
     * @param {SkyLevel} level 
     */
    constructor(level) {
        super(level);
        this.level.levelAnnouncement = new Announcement("Sky");
        this.level.levelAnnouncement.Start();
    }

    UpdateSprites() {
        super.UpdateSprites();
        if (this.level.dragon.parts[0].body.left < 0) {
            this.level.state = new SkyLevelFadeoutState(this.level);
        }
    }
}

class SkyLevelFadeoutState extends SkyLevelState {

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @param {SkyLevel} level 
     */
    constructor(level) {
        super(level);
        let layers = [];
        for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
            layers.push(i);
        }
        this.fadeout = new Fadeout(1000, layers, () => {
            this.level.Stop();
            this.level.nextScene = this.level.nextSceneOnExit;
            this.level.nextSceneOnExit.Start();
        });
    }

    UpdateSprites() {
        super.UpdateSprites();
        this.fadeout.Update();
    }

}

export default class SkyLevel extends Scene {

    /**
     * @type SkyLevelState
     */
    state;

    /**
     * @type Dragon
     */
    dragon;

    /**
     * @type Array<Ring>
     */
    rings;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @param {Scene} nextSceneOnExit 
     */
    constructor(nextSceneOnExit) {
        super();
        this.pausable = false;
        this.nextSceneOnExit = new EndingScene(nextSceneOnExit);
    }

    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "red_ring":
                        this.rings.push(new Ring(EntityColor.RED, x, y));
                        break;
                    case "green_ring":
                        this.rings.push(new Ring(EntityColor.GREEN, x, y));
                        break;
                    case "blue_ring":
                        this.rings.push(new Ring(EntityColor.BLUE, x, y));
                        break;
                    case "yellow_ring":
                        this.rings.push(new Ring(EntityColor.YELLOW, x, y));
                        break;
                    case "capuchine_on_dragon":
                        if (!this.dragon) {
                            this.dragon = new Dragon(x, y);
                        }
                }
            },
            map);
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap("maps/sky/sky.tmx");

        Playnewton.PPU.SetWorldBounds(0, 0, 8192, 576);
        Playnewton.PPU.SetWorldGravity(0, 0);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    async InitDialog() {
        await CapuchinePortrait.Load();
        await DragonPortrait.Load();
    }

    async Start() {
        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            //TODO
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 20;

        await Ring.Load();
        this.rings = [];
        this.progress = 30;

        await Dragon.Load();
        this.progress = 40;

        await this.InitDialog();
        this.progress = 60;

        await this.InitMap();
        this.progress = 80;

        await audioPromise;
        this.progress = 100;

        this.state = new SkyLevelFlyState(this);
    }

    Stop() {
        super.Stop();

        Dragon.Unload();
        this.dragon = null;

        Ring.Unload();
        this.rings = [];

        CapuchinePortrait.Unload();
        DragonPortrait.Unload();

        this.state = null;
    }

    UpdateBodies() {
        this.state?.UpdateBodies();
    }

    UpdateSprites() {
        this.state?.UpdateSprites();
    }
}
