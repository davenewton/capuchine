import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import Capuchine from "../entities/capuchine.js"
import Fadeout from "../entities/fadeout.js"
import Turtle from "../entities/turtle.js"
import { Dialog } from "../entities/dialog.js"
import CapuchinePortrait from "../entities/capucine_portrait.js"
import TurtlePortrait from "../entities/turtle_portrait.js"

class BeachLevelState {
    /**
     * @type BeachLevel
     */
    level;

    /**
     * @param {BeachLevel} level 
     */
    constructor(level) {
        this.level = level;
    }

    UpdateBodies() {
        this.level.capuchine.UpdateBody();
        this.level.turtle.UpdateBody();
    }

    UpdateSprites() {
        this.level?.levelAnnouncement.Update();
        this.level.capuchine.UpdateSprite();
        this.level.turtle.UpdateSprite();
    }
}

class BeachLevelHelpTurtleState extends BeachLevelState {
    
    /**
     * @param {BeachLevel} level 
     */
     constructor(level) {
        super(level);
        this.level.levelAnnouncement = new Announcement("Beach");
        this.level.levelAnnouncement.Start();
    }

    UpdateSprites() {
        super.UpdateSprites();
        if(this.level.turtle.talking) {
            this.level.state = new BeachLevelDialogState(this.level);
        }
    }
}

class BeachLevelDialogState extends BeachLevelState {

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Dialog
     */
    dialog;
    
    /**
     * @param {BeachLevel} level 
     */
     constructor(level) {
        super(level);

        let capuchinePortrait = new CapuchinePortrait();
        let turtlePortrait = new TurtlePortrait();

        this.dialog = new Dialog();

        this.dialog.Start([
            { color: "#adb834", text: "[Turtle] Thank you !", portrait: turtlePortrait, delay: 5000 },
            { color: "#bc4a9b", text: "[Capuchine] You're welcome !", portrait: capuchinePortrait, delay: 5000 },
            { color: "#adb834", text: "[Turtle] I'm going back to the Dragon Palace.", portrait: turtlePortrait, delay: 5000},
            { color: "#bc4a9b", text: "[Capuchine] I'd love to see it !", portrait: capuchinePortrait, delay: 5000 },
            { color: "#adb834", text: "[Turtle] Get on my back !", portrait: turtlePortrait, delay: 5000 }
        ]);

        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
        Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
    }

    UpdateSprites() {
        super.UpdateSprites();
        let pad = Playnewton.CTRL.GetMasterPad();
        if (pad.TestStartAndResetIfPressed()) {
            Playnewton.APU.PlaySound("sounds/skip.wav");
            this.dialog.Skip();
        }
        this.dialog.Update(this.dialogLabel);
        if(this.dialog.done) {
            this.level.state = new BeachLevelFadeoutState(this.level);
        }
    }

}

class BeachLevelFadeoutState extends BeachLevelState {

    /**
     * @type Fadeout
     */
    fadeout;
    
    /**
     * @param {BeachLevel} level 
     */
     constructor(level) {
        super(level);
        let layers = [];
        for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
            layers.push(i);
        }
        this.fadeout = new Fadeout(1000, layers, () => {
            this.level.Stop();
            this.level.nextScene = this.level.nextSceneOnExit;
            this.level.nextSceneOnExit.Start();
        });
    }

    UpdateSprites() {
        super.UpdateSprites();
        this.fadeout.Update();
    }

}

export default class BeachLevel extends Scene {

    /**
     * @type BeachLevelState
     */
    state;

    /**
     * @type Capuchine
     */
    capuchine;

    /**
     * @type Turtle
     */
    turtle;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @param {Scene} nextSceneOnExit 
     */
    constructor(nextSceneOnExit) {
        super();
        this.pausable = false;
        this.nextSceneOnExit = nextSceneOnExit;       
    }

    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "turtle":
                        if (!this.turtle) {
                            this.turtle = new Turtle(x, y);
                        }
                        break;
                    case "capuchine":
                        if (!this.capuchine) {
                            this.capuchine = new Capuchine(x, y);
                        }
                        break;
                }
            },
            map);
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap("maps/beach/beach.tmx");

        Playnewton.PPU.SetWorldBounds(192, 0, 832, 576);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    async InitDialog() {
        await CapuchinePortrait.Load();
        await TurtlePortrait.Load();
    }

    async Start() {
        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            //TODO
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 20;

        await Capuchine.Load();
        this.progress = 30;

        await Turtle.Load();
        this.progress = 40;

        await this.InitDialog();
        this.progress = 60;

        await this.InitMap();
        this.progress = 80;

        await audioPromise;
        this.progress = 100;

        this.state = new BeachLevelHelpTurtleState(this);
    }

    Stop() {
        super.Stop();

        Capuchine.Unload();
        this.capuchine = null;

        CapuchinePortrait.Unload();
        TurtlePortrait.Unload();

        Turtle.Unload();
        this.turtle = null;

        this.state = null;
    }

    UpdateBodies() {
        this.state?.UpdateBodies();
    }

    UpdateSprites() {
        this.state?.UpdateSprites();
    }
}
