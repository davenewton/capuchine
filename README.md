MOVED TO https://codeberg.org/devnewton/capuchine

# capuchine

Simple 2d game for children.

![Screenshot](docs/screenshot.png)

## Licenses

Unless stated otherwise, everything is under MIT license.

## Acknowledgements

Thanks to [Libresprite](http://www.libresprite.org/) developers. I use it to draw my sprites.

![Libresprite](docs/libresprite.png)

Thanks to [jsfxr](https://sfxr.me/) authors, I use it for sound effects.

![jsfxr](docs/jsfxr.png)

Thanks to [Tiled](https://www.mapeditor.org/) developers. I use it for level edit.

![tiled](docs/tiled.png)
